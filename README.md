A simple jQuery project to present issues like stories on a scrum like board. The project communicate via Gitlab API and creates a side project to keep track of tasks.
> See wiki for screenshot

# Howto
1. Generate a token
2. Init the task board in Gitlab. This project uses the same lanes but also adds a Todo lane
3. Open js/parameters.js
4. Set token from 1.
5. Set address to Gitlab server including /api/v3
6. Set projectId from Gitlab (used when accessing project data)
7. Enter current milestone string.
8. Open gitlabTasks.html in a browser (Only tested in latest Chrome)

## Create task
1. Click the add task button in the Todo lane.
2. Enter task name and enter more information if wanted
3. Click Add task. This will create an issue in the __StoryTasks project including one row of JSON data

## Add a story/issue
1. Goto Gitlab project
2. Create an issue with chosen milestone