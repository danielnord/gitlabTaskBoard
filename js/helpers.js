function Helpers() {
	"use strict";
	
	this.isJson = function(text) {
		var result = /^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
replace(/(?:^|:|,)(?:\s*\[)+/g, ''));

		if (result) {
			// Check if object when parsed
			if (text === "" || getType(JSON.parse(text)) !== "Object") {
				result = false;
			}
		}
		
		return result;
	}
	
	 function getType(object) {
		var stringConstructor = "test".constructor;
		var arrayConstructor = [].constructor;
		var objectConstructor = {}.constructor;

		if (object === null) {
			return "null";
		}
		else if (object === undefined) {
			return "undefined";
		}
		else if (object.constructor === stringConstructor) {
			return "String";
		}
		else if (object.constructor === arrayConstructor) {
			return "Array";
		}
		else if (object.constructor === objectConstructor) {
			return "Object";
		}
		else {
			return "N/A";
		}
	}
	
	
	
	// Converts hours to duration in human format. e.g: 3h30m
	this.toHumanFormat = function(hours) {
		var h = Math.floor(hours);
		var m = parseInt(hours % 1.0*60);
		return h + "h" + m + "m";
	}
	
	this.toDuration = function(seconds) {
		return (seconds / 3600).toFixed(2);
		/*
		var p = humanFormat.split("h");
		var h = p[0];
		var m = parseInt(p[1].replace("m","")) / 60;
		return parseFloat(h + "." + m).toFixed(2);
		*/
	}
	
	// Returns light or dark class depending on backgroundColor
	this.getFontClass = function(backgroundColor) {
		if (!backgroundColor) {
			return "dark";
		}
		
		var r = hexToR(backgroundColor);
		var g = hexToG(backgroundColor);
		var b = hexToB(backgroundColor);

		var uicolors = [r / 255, g / 255, b / 255];
		var c = uicolors.map((c) => {
		  if (c <= 0.03928) {
			return c / 12.92;
		  } else {
			return Math.pow((c + 0.055) / 1.055,2.4);
		  }
		});

		var L = 0.2126 * c[0] + 0.7152 * c[1] + 0.0722 * c[2];
		return (L > 0.179) ? 'dark' : 'light';
	};
	 
	function cutHex(h) {
		return (h.charAt(0) == "#") ? h.substring(1, 7) : h
	};
	function hexToR(h) {
		return parseInt((cutHex(h)).substring(0, 2),16)
	  };
	function hexToG(h) {
		return parseInt((cutHex(h)).substring(2, 4),16)
	};
	function hexToB(h) {
		return parseInt((cutHex(h)).substring(4, 6),16)
	};
	
	
}