function Toolbar(gitlabMilestone) {
    "use strict";	
	var self = this;
	this.init = function(callback) {
		gitlabMilestone.list(function(milestones){
			var html = "";
			var id = "milestone";
			html += '<div class="milestones">';
			html += '<div id="' + id + '">';
			html += gitlabMilestone.getMilestoneList(milestones);
			html += '</div>';
			html += '</div>';
			
			$("header.toolbar").html(html);
			$("#" + id + " .dropdown-toggle-text").text(gitlabMilestone.currentMilestone);
			
			$("#" + id + " .dropdown-menu-toggle").on("click", function(e){
				$("#" + id + " .dropdown-menu").toggle();
				
				$("#" + id + " .dropdown-menu ul li").off().on("click", function(e) {
					$("#" + id + " .dropdown-menu").toggle();
					
					var value = $(this).find("a").text().trim();
					$("#" + id + "").data("value",value);
					$("#" + id + " .dropdown-toggle-text").text(value);
					gitlabMilestone.setCurrentMilestone(value);
					self.milestoneChanged(value);
				});
			});
			
			callback();
		});
		
		self.milestoneChanged(gitlabMilestone.currentMilestone); // TODO: Use stored milestone
	}
	
	this.milestoneChanged = null;
};