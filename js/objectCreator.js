function ObjectCreator(GITLAB) {
    "use strict";	
	
	var gitlabTask = null; // will be fetched in init
	var gitlabUser = null; // will be fetched in init
	var gitlabIssue = null; // will be fetched in init
	var gitlabLabel = null; // will be fetched in init
	var gitlabMilestone = null; // will be fetched in init
	var gitlabBoard = null; // will be fetched in init

	this.parameters = GITLAB;
	
	this.gitlabLabel = function() {
		return gitlabLabel;
	}
	
	this.gitlabUser = function() {
		return gitlabUser;
	}
	
	
	var initialized = {}; // Empty object that will be filled with initialized Gitlab objects
	
	/*
	@initialized an object keeping the objects that has been initialized
	*/
	this.init = function(callback) {
			
		initGitlabTask(this, function() {
			initialized.task = gitlabTask;
			callback(initialized);
		});
		
		initGitlabUser(this, function() {
			initialized.user = gitlabUser;
			callback(initialized);
		});	
		
		gitlabIssue = new GitlabIssue(this, GITLAB.projectId);
		initialized.issue = gitlabIssue;
		callback(initialized);
		
		initGitlabLabel(this, function() {
			initialized.label = gitlabLabel;
			callback(initialized);
		});
		
		initGitlabMilestone(this, function() {
			initialized.milestone = gitlabMilestone;
			callback(initialized);
		});
		
		initGitlabBoard(this, function() {
			initialized.board = gitlabBoard;
			callback(initialized);
		});
	}
	
	function initGitlabTask(self, callback) {
		gitlabTask = new GitlabTask(self);
		var gitlabProject = new GitlabProject(self);
		gitlabProject.getProjectById(GITLAB.projectId, function(project) {
			var namespace = project.namespace;
			gitlabTask.init(project.id, namespace, function(){
				callback();
			});
		});
	}
	
	function initGitlabUser(self, callback) {
		gitlabUser = new GitlabUser(self);
		gitlabUser.init(function() {
			callback();
		});	
	}
	
	function initGitlabLabel(self, callback) {
		gitlabLabel = new GitlabLabel(self);
		gitlabLabel.list(self.parameters.projectId, function(ls) {
			callback();
		});	
	}
	
	function initGitlabMilestone(self, callback) {
		gitlabMilestone = new GitlabMilestone(self);
		callback();
	}
	
	function initGitlabBoard(self, callback) {
		gitlabBoard = new GitlabBoard(self);
		gitlabBoard.list(function(boards) {
			callback();
		});
	}
};