function GitlabTask(objectCreator) {
    "use strict";
	var gitlabTaskProjectId = null;
	var gitlabIssue = null;
	var namespacePath = null;
	this.init = function(projectId, namespace, callback) {
		namespacePath = namespace.path;
		var gitlabProject = new GitlabProject(objectCreator);
		
		gitlabProject.getTaskProject(namespacePath, function(taskProject){
			gitlabTaskProjectId = taskProject.id;
			gitlabIssue = new GitlabIssue(objectCreator, taskProject.id);
			// Sync labels
			var gitlabLabel = new GitlabLabel(objectCreator);
			var fromProject = projectId;
			var toProject = taskProject.id;
			gitlabLabel.syncLabels(fromProject, toProject, function(){
				callback();
			});
		});
	}
	
	this.showAddDialog = function(milestoneLabel, label, storyId, parentTaskId, callback) {
		emptyTaskDialog();
		$("#modalAddTask button.add").text("Add task");
		$("#modalAddTask .modal-title").text("Add task");
		$("#modalAddTask").modal("toggle");
		
		$("#modalAddTask button.add").off().on("click", function() {
			var gitlabMilestone = new GitlabMilestone(objectCreator, gitlabTaskProjectId);
			gitlabMilestone.fetch(objectCreator.parameters.milestone, function(milestone){
				var title = $("#task-title").val();
				var description = $("#task-description").val();
				var duration = new Helpers().toHumanFormat($("#task-estimatedHours").val());
				var taskData = {
					storyId: storyId,
					parentTaskId: parentTaskId
				};
				label += "," + $("#task-label").data("value");
				update(null, title, description, duration, taskData, milestone.id, label, true, function(task) {
					callback(task);
					$("#modalAddTask").modal("toggle");
				});
			});
		});
	}
	
	this.showEditDialog = function(taskId, label, callback) {
		emptyTaskDialog();
		$("#modalAddTask .modal-title").text("Edit task");
		$("#modalAddTask button.add").text("Save changes");
		$("#modalAddTask").modal("toggle");
		
		var currentTask = null;
		this.get(taskId, function(task){
			currentTask = task;
			
			$("#task-title").val(task.title);
			$("#task-description").val(task.description);
			
			var estimatedHours = new Helpers().toDuration(task.timeStatistics.time_estimate);
			$("#task-estimatedHours").val(estimatedHours);
			
			// Label
			var lane = getLaneLabel(task);
			var labelsToSplice = task.labels.slice();
			if (lane) {
				labelsToSplice.splice(lane.index, 1);
			}
			
			$("#task-label").data("value", labelsToSplice[0]);
			$("#task-label .dropdown-toggle-text").text(labelsToSplice[0]);
		});
		
		$("#modalAddTask button.add").off().on("click", function() {
			var gitlabMilestone = new GitlabMilestone(objectCreator, gitlabTaskProjectId);
			
			currentTask.title = $("#task-title").val();
			currentTask.description = $("#task-description").val();
			currentTask.duration = new Helpers().toHumanFormat($("#task-estimatedHours").val());
			currentTask.labelName = $("#task-label").data("value");
			
			// Label
			var lane = getLaneLabel(currentTask);
			currentTask.labels = new Array();
			currentTask.labels[0] = lane.title;
			
			edit(currentTask, false, function(task) {
				callback(task);
				$("#modalAddTask").modal("toggle");
				currentTask = null;
			});
		});
	}
	
	// Set taskDialog to default state
	function emptyTaskDialog() {
		$("#modalAddTask button.add")
		$("#task-title").val("");
		$("#task-description").val("");
		$("#task-estimatedHours").val("8.0");
		$("#task-label .dropdown-toggle-text").text("Labels");
		
		var labels = objectCreator.gitlabLabel().list(null);
		$("#task-label").html(objectCreator.gitlabLabel().getLabelList(labels));
		
		$("#task-label .dropdown-menu-toggle").on("click", function(e){
			$("#task-label .dropdown-menu").toggle();
			
			$("#task-label .dropdown-menu ul li").off().on("click", function(e) {
				$("#task-label .dropdown-menu").toggle();
				
				var value = $(this).find("a").text().trim();
				$("#task-label").data("value",value);
				$("#task-label .dropdown-toggle-text").text(value);
			});
		});
	}
	
	function getLaneLabel(task) {
		var lanes = $(".board .titles div");
		for (var i in lanes) {
			for (var j in task.labels) {
				if (task.labels[j] === $(lanes[i]).text()) {
					return {
						index: parseInt(j),
						title: task.labels[j]
					};
				}
			}
		}
		
		return null;
	}
	
	this.setParent = function(taskId, parentId, storyId, lane) {
		this.get(taskId, function(task){
				
			var setAsAssignee = true;
			for (var i in task.labels) {
				if (task.labels[i] === lane) {
					setAsAssignee = false; // Do not change assignee if a user moves a task inside the same lane
					break;
				}
			}
			task.labelName = lane;
			task.taskData.storyId = storyId;
			
			// Remove lanes from labels
			$(".board .titles div").each(function(i, e) {
				var index = task.labels.indexOf($(e).text());
				if (index > -1) {
					task.labels.splice(index, 1);
				}
			});
			
			
			if (parentId) {
				task.taskData.parentTaskId = parseInt(parentId, 10);
			}
			else {
				task.taskData.parentTaskId = null;
			}
			
			
			edit(task, setAsAssignee, function(){});
		});
	}
	
	this.edit = function(task, setAsAssignee, callback) {
		edit(task, setAsAssignee, callback);
	}
	
	function edit(task, setAsAssignee, callback) {
		var labels = task.labels.join(",");
		if (task.labelName && labels.indexOf(task.labelName) < 0) {
			labels += "," + task.labelName;
		}
		update(task.id, task.title, task.description, task.duration, task.taskData, task.milestone.id, labels, setAsAssignee, callback);
	}
	
	// Lists all tasks.
	// @ignoreCache Ignore cached data
	// @callback(task)
	this.list = function(ignoreCache, callback) {
		gitlabIssue.list(ignoreCache, null, null, function(issues) {
			for (var i in issues) {
				parseTask(issues[i]);
			}
			
			callback(issues);
		});
	}
	
	this.get = function(taskId, callback) {
		gitlabIssue.get(taskId, function(issue) {
			parseTask(issue);
			callback(issue);
		});
	}
	
	// Parse task description before sending it outside class
	function parseTask(task) {
		var parsed = parseDescription(task.description);
		task.description = parsed.description;
		task.taskData = parsed.data;
	}
	
	//Add or edit a task. Set id to null to add new
	function update(taskId, title, description, duration, taskData, milestoneId, labels, setAsAssignee, callback) {
		var parsed = parseDescription(description);
		parsed.data = taskData;
		
		var assigneeId = null;
		if (setAsAssignee) {
			assigneeId = objectCreator.gitlabUser().getCurrentUser().id;
		}
		
		var issue = {
			id: taskId,
			title: title,
			description: setExtendedData(parsed.description, parsed.data),
			milestoneId: milestoneId,
			labels: labels,
			assigneeId: assigneeId,
			duration: duration
		};
		gitlabIssue.update(issue, function(task) {
			parseTask(task);
			callback(task);
		});
	}
	
	// The description keeps extra data about which story it's connected to and in which order the task should be displayed in lane
	function parseDescription(description) {
		if (description === null) {
			return {
				description: null,
				data: {storyId: 0}
			};
		}
		var rows = description.split(/\r?\n/);
		var lastRow = rows[rows.length - 1];
		var result = null;
		
		if (new Helpers().isJson(lastRow)) {
			rows.splice(-1,1);
			var concat = rows.join("\r\n");
			result = {
				description: concat,
				data: JSON.parse(lastRow)
			};
		}
		else {
			result = {
				description: description,
				data: {storyId: 0}
			};
		}
		
		return result;
	}
	
	function setExtendedData(description, data) {
		description += "\r\n";
		description += JSON.stringify(data);
		return description;
	}	
};