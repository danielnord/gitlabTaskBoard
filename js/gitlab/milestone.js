function GitlabMilestone(objectCreator) {
    "use strict";	
	this.currentMilestone = window.localStorage.getItem("currentMilestone") ? window.localStorage.getItem("currentMilestone") : objectCreator.parameters.milestone;
	objectCreator.parameters.milestone = this.currentMilestone;
	
	var cachedMilestones = null;
	
	this.fetch = function(title, callback) {
		this.list(function(milestones){
			// Find project of type
			
			for (var m in milestones) {
				var milestone = milestones[m];
				if (milestone.title === title) {
					callback(milestone);
					return;
				}
			}
			
			// Create milestone if not found
			create(title, callback);
		});
	}
	
	this.list = function(callback) {
		if (cachedMilestones) {
			if (callback) {
				callback(cachedMilestones);
			}
			return cachedMilestones;
		}
		var cacheKey = "gitlab-milestones-" + objectCreator.parameters.projectId;
		var localCache = window.localStorage.getItem(cacheKey);
		if (localCache) {
			callback(JSON.parse(localCache));
			callback = null;
		}
		
		list(function(milestones){
			cachedMilestones = milestones;
			window.localStorage.setItem(cacheKey, JSON.stringify(milestones));
			if (callback) {
				callback(milestones);
			}
		});
	}
	
	this.create = function(title, callback) {
		create(title, callback);
	}
	
	// creates a dropdown based on milestones
	this.getMilestoneList = function(milestones) {
		var result = "";
		
		result += '<div class="selectbox">';
		result += '<div class="dropdown open">';
		result += '<button class="dropdown-menu-toggle js-milestone-select">';
		result += '<span class="dropdown-toggle-text">';
		result += 'Milestones';
		result += '</span>';
		result += '<i class="glyphicon glyphicon-chevron-down"></i>'
		result += '</button>';
		
		
		result += '<div class="dropdown-menu dropdown-select dropdown-menu-milestones dropdown-menu-selectable" style="display: none;">';
		result += '  <div class="dropdown-page-one">';
		result += '    <div class="dropdown-title">Select  milestone</div>';
		result += '    <div class="dropdown-content"><ul>\r\n';
		if (milestones) {
			for (var i in milestones) {
				result += '    <li class="' + milestones[i].state + '"><a>\r\n';
				result += milestones[i].title;			
				result += '    </a></li>\r\n';
			}
		}
		
		result += '    </ul></div>';
		result += '  </div>';
		result += '</div>';
		
		result += '</div>';
		result += '</div>';
		return result;
	}
	
	this.setCurrentMilestone = function(milestone) {
		objectCreator.parameters.milestone = milestone;
		window.localStorage.setItem("currentMilestone", milestone);
		this.currentMilestone = milestone;
	}
	
	function list(callback) {
		var path = "/projects/" + objectCreator.parameters.projectId + "/milestones";
		var url = new GitlabServer(objectCreator.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(labels) {
			callback(labels);
		});
	}
	
	function create(title, callback) {
		var path = "/projects/" + objectCreator.parameters.projectId + "/milestones";
		path += "?title=" + title;				
		var url = new GitlabServer(objectCreator.parameters).url(path);
		
		$.ajax({
			type: "post",
			url: url
		}).then(function(data) {
			console.log(data);
			callback(data);
		});
	}
};