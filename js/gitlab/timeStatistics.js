function GitlabTimeStatistics(parentStoryboard, projectId) {
	"use strict";
	
	// @issueId	The ID of a project's issue
	// @duration The duration in human format. e.g: 3h30m
	this.set = function(issueId, duration, callback) {
		if (!duration) {
			callback(null);
			return;
		}
		set(issueId, duration, callback);
	}
	
	// @issueId	The ID of a project's issue
	this.get = function(issueId, callback) {
		get(issueId, callback);
	}
	
	// Lists all stats by sending a callback for each id
	// @issueIds [issueId]
	// @callback(issueId, duration)
	this.list = function(issueIds, callback) {
		var issues = new Array();
		var left = issueIds.length;
		for(var i in issueIds) {
			get(issueIds[i], function(issueId, stats) {
				issues[issues.length] = {
					id: issueId, 
					timeStatistics: stats
				};
				left--;
				if (left <= 0) {
					callback(issues);
				}
			});
		}
	}
	
	// Lists all stats by sending a callback for each id
	// @issueIds [issueId]
	// @callback(issueId, duration)
	this.listEach = function(issueIds, callback) {
		for(var i in issueIds) {
			get(issueIds[i], callback);
		}
	}
	
	function set(issueId, duration, callback) {
		var path = "/projects/" + projectId + "/issues";
		path += "/" + issueId + "/time_estimate";
		path += "?duration=" + duration;
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			type: "post",
			url: url
		}).then(function(stats) {
			callback(stats);
		});
	}
	
	function get(issueId, callback) {
		var path = "/projects/" + projectId + "/issues";
		path += "/" + issueId + "/time_stats";
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(stats) {
			callback(issueId, stats);
		});
	}
	
};