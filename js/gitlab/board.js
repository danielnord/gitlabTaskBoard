function GitlabBoard(objectCreator) {
    "use strict";	
	
	var cachedBoards = null;
	
	this.list = function(callback) {
		if (cachedBoards) {
			if (callback) {
				callback(cachedBoards);
			}
			return cachedBoards;
		}

		var localCache = window.localStorage.getItem("gitlab-boards");
		if (localCache) {
			callback(JSON.parse(localCache));
			callback = null;
		}
		
		list(function(boards){
			cachedBoards = boards;
			window.localStorage.setItem("gitlab-boards", JSON.stringify(boards));
			if (callback) {
				callback(boards);
			}
		});
	}
	
	function list(callback) {
		var path = "/projects/" + objectCreator.parameters.projectId + "/boards";
		var url = new GitlabServer(objectCreator.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(boards) {
			callback(boards);
		});
	}
};