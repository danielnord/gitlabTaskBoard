function GitlabLabel(objectCreator) {
    "use strict";	
	
	var cachedLabels = null;
	
	this.fetch = function(name, callback) {
		list(function(labels){
			
			// Find project of type			
			for (var l in labels) {
				var label = labels[l];
				if (label.name === name) {
					callback(label);
					return;
				}
			}
			
			callback(null);
		});
	}
	
	this.list = function(projectId, callback) {
		"use strict";
		
		if (cachedLabels) {
			if (callback) {
				callback(cachedLabels);
			}
			return cachedLabels;
		}

		var cacheKey = "gitlab-labels-" + projectId;
		var localCache = window.localStorage.getItem(cacheKey);
		if (localCache) {
			callback(JSON.parse(localCache));
			callback = null;
		}
		
		list(projectId, function(labels){
			cachedLabels = labels;
			window.localStorage.setItem(cacheKey, JSON.stringify(labels));
			if (callback) {
				callback(labels);
			}
		});
	}
	
	this.syncLabels = function(fromProjectId, toProjectId, callback) {
		var otherLabel = new GitlabLabel(objectCreator);
		otherLabel.list(fromProjectId, function(otherProjectLabels) {
			list(toProjectId, function(labels) {
				for (var i1 in otherProjectLabels) {
					var found = false;
					for (var i2 in labels) {
						if (labels[i2].name === otherProjectLabels[i1].name) {
							found = true;
						}
					}
					
					// add label if not found in iteration
					if (!found) {
						create(toProjectId, otherProjectLabels[i1], function(label){
							console.log(label);
						});
					}
				}
				
				callback();
			});
			
		});
	}
	
	this.fetchFromList = function(label, labels) {
		for (var i in labels) {
			if (labels[i].name === label) {
				return labels[i];
			}
		}
		return null;
	}
	
	// creates a dropdown based on labels
	this.getLabelList = function(labels) {
		var result = "";
		
		result += '<div class="selectbox">';
		result += '<div class="dropdown open">';
		result += '<button class="dropdown-menu-toggle js-label-select">';
		result += '<span class="dropdown-toggle-text">';
		result += 'Labels';
		result += '</span>';
		result += '<i class="glyphicon glyphicon-chevron-down"></i>'
		result += '</button>';
		
		
		result += '<div class="dropdown-menu dropdown-select dropdown-menu-labels dropdown-menu-selectable" style="display: none;">';
		result += '  <div class="dropdown-page-one">';
		result += '    <div class="dropdown-title">Select  label</div>';
		result += '    <div class="dropdown-content"><ul>\r\n';
		if (labels) {
			for (var i in labels) {
				result += '    <li><a>\r\n';
				result += '    <span class="dropdown-label-box" style="background: ' + labels[i].color + '"></span>\r\n';
				result += labels[i].name;			
				result += '    </a></li>\r\n';
			}
		}
		
		result += '    </ul></div>';
		result += '  </div>';
		result += '</div>';
		
		result += '</div>';
		result += '</div>';
		return result;
	}
	
	function list(projectId, callback) {
		"use strict";
		var path = "/projects/" + projectId + "/labels";
		var url = new GitlabServer(objectCreator.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(labels) {
			callback(labels);
		});
	}
	
	function create(projectId, label, callback) {
		"use strict";
		var path = "/projects/" + projectId + "/labels";
		path += "?name=" + encodeURIComponent(label.name);
		
		if (label.color === null) {
			label.color = "#fff";
		}
		
		path += "&color=" + encodeURIComponent(label.color);
		
		if (label.description !== null) {
			path += "&description=" + encodeURIComponent(label.description);
		}
		var url = new GitlabServer(objectCreator.parameters).url(path);
		
		
		$.ajax({
			type: "post",
			url: url
		}).then(function(data) {
			console.log(data);
			callback(data);
		});
	}
};