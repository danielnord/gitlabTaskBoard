function GitlabProject(parentStoryboard) {
	"use strict";
	
	var storyTasksProjectName = parentStoryboard.parameters.storyTasksProjectName;
	this.getProject = function(namespacePath, projectName, callback) {
		getProject(namespacePath, projectName, callback);		
	}
	
	this.getProjectById = function(projectId, callback) {
		var path = "/projects/" + projectId;
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(project) {	
			callback(project);
		});
	}
	
	this.getTaskProject = function(namespacePath, callback) {
		var project = getProject(namespacePath, storyTasksProjectName, function(project){
			if (project === null) {
				// Project not found. Let's create it
				createProject(namespacePath, function(project){
					getProject(namespacePath, storyTasksProjectName, callback);
				});
			}
			else {
				callback(project);
			}
		});
		
	}
	
	// Input namespacePath (example: 002-83)
	function getProject(namespacePath, projectName, callback) {
		var path = "/projects/search/" + namespacePath;
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(projects) {			
			// Find project of type
			for (var p in projects) {
				var project = projects[p];
				if (project.name === projectName) {
					callback(project);
					return;
				}
			}
			
			callback(null);
		});
	}
	
	function getNamespaceId(namespacePath, callback) {
		var path = "/groups?search=" + namespacePath;
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(groups) {			
			// Find project of type
			
			for (var g in groups) {
				var group = groups[g];
				callback(group.id);
			}
			
			callback(null);
		});
	}
	
	function createProject(namespacePath, callback) {
		getNamespaceId(namespacePath, function(namespaceId) {
			if (namespaceId === null) {
				return;
			}
			
			var path = "/projects/?name=" + storyTasksProjectName + "&namespace_id=" + namespaceId;
			var url = new GitlabServer(parentStoryboard.parameters).url(path);
			
			
			$.ajax({
				type: "post",
				url: url
			}).then(function(data) {
				callback(data);
			});
		});
		
	}
}