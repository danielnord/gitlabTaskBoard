function GitlabIssue(parentStoryboard, projectId) {
	"use strict";
	
	var gitlabTimeStatistics = new GitlabTimeStatistics(parentStoryboard, projectId);
	
	/*
	@issue = {
			title: title,
			description: setExtendedData(parsed.description, parsed.data),
			milestoneId: milestoneId,
			labels: labelName,
			assigneeId: arentStoryboard.gitlabUser().getCurrentUser().id,
			duration: duration
		};
	*/
	this.add = function(issue, callback) {
		update(issue, callback);
	}
	
	/*
	@issue = {
			issueId: issueId
			title: title,
			description: setExtendedData(parsed.description, parsed.data),
			milestoneId: milestoneId,
			labels: labelName,
			assigneeId: arentStoryboard.gitlabUser().getCurrentUser().id,
			duration: duration
		};
	*/
	this.update = function(issue, callback) {
		update(issue, callback);
	}
	
	// @issueId	The ID of a project's issue
	this.get = function(issueId, callback) {
		get(issueId, callback);
	}	
	
	// Lists all issues.
	// @ignoreCache Ignore cached data
	// @callback(task)
	this.list = function(ignoreCache, state, milestone, callback) {
		var cacheName = "gitlab-issues-" + state + "-" + milestone;

		var localCache = window.localStorage.getItem(cacheName);
		if (localCache && !ignoreCache) {
			callback(JSON.parse(localCache));
			callback = null;
		}
		
		list(state, milestone, function(issues) {
			window.localStorage.setItem(cacheName, JSON.stringify(issues));
			
			if (callback) {
				callback(issues);
			}
		});
	}
	
	//Add or edit an issue. Set id to null to add new
	function update(issue, callback) {
		var path = "/projects/" + projectId + "/issues";
	
		var verb = "post";
		if (issue.id !== null) {
			verb = "put";
			path += "/" + issue.id;
		}
		
		path += "?title=" + issue.title;
		path += "&milestone_id=" + issue.milestoneId;
		
		if (issue.assigneeId) {
			path += "&assignee_id=" + issue.assigneeId;
		}
		
		path += "&labels=" + issue.labels;
		path += "&description=" + encodeURIComponent(issue.description);
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			type: verb,
			url: url
		}).then(function(addedIssue) {
			// Update time estimation
			gitlabTimeStatistics.set(addedIssue.id, issue.duration, function(stats){
				addedIssue.timeStatistics = stats;
				callback(addedIssue);	
			});
		});
	}
	
	function get(issueId, callback) {
		var path = "/projects/" + projectId + "/issues";
		path += "/" + issueId;
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(issue) {			
			gitlabTimeStatistics.get(issue.id, function(id, stats) {
				issue.timeStatistics = stats;
				callback(issue);
			});
		});
	}
	
	function list(state, milestone, callback) {
		var path = "/projects/" + projectId + "/issues?";
		
		if (state) {
			path += "&state=" + state;
		}
		if (milestone) {
			path += "&milestone=" + milestone;
		}
		
		var url = new GitlabServer(parentStoryboard.parameters).url(path);
		
		$.ajax({
			url: url
		}).then(function(issues) {
			// Fetch time statistics
			var taskIds = issues.map(function(elem){
				return elem.id;
			});				
			gitlabTimeStatistics.list(taskIds, function(statistics) {
				for (var i in issues) {
					for (var j in statistics) {
						if (issues[i].id === statistics[j].id) {
							issues[i].timeStatistics = statistics[j].timeStatistics;
						}
					}
				}
				
				callback(orderIssues(issues));
			});
		});
	}
	
	// Order issues by state
	function orderIssues(issues) {
		var opened = new Array();
		var closed = new Array();
		for (var i in issues) {
			if (issues[i].state === "opened") {
				opened[opened.length] = issues[i];
			}
			else {
				closed[closed.length] = issues[i];
			}
		}
		
		return opened.concat(closed);
	}
	
};