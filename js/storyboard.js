function Storyboard(GITLAB, gitlabTask, gitlabUser, gitlabIssue, gitlabLabel, gitlabBoard, board) {
    "use strict";
	
	var TileType = {
		STORY: 1,
		TASK: 2
	}
	
	GITLAB.todoLaneName = "Todo"; // TODO: Update parameters.js
	
	this.parameters = GITLAB;

	this.init = function(callback) {
		redraw(callback);
	}
	
	this.redraw = function(callback) {
		redraw(callback);
	}
	
	this.updateTasks = function() {
		updateTasks(false);
	}
	
	// Update task position at server. Three 
	this.updateTaskPosition = function(taskId) {
		var taskElement = $("#" + taskId);
		var storyId = taskElement.parent().parent().parent().data("story");
		var lane = taskElement.parent().data("label");
		
		var previous = taskElement.prev();
		var next = taskElement.next();
		
		// Update previous if exists. toolbar class is always the first element
		if (previous.attr("class") !== "toolbar") {
			var previousParentId = null; // noparent
			if (previous.prev().attr("class") !== "toolbar") {
				previousParentId = previous.prev().attr("id");
			}
			gitlabTask.setParent(previous.attr("id"), previousParentId, storyId, lane);
		}
		
		// Update current
		gitlabTask.setParent(taskId, previous.attr("id"), storyId, lane);
		
		// Update next if exists
		if (next.length > 0) {
			gitlabTask.setParent(next.attr("id"), taskId, storyId, lane);
		}
	}
	
	this.findParent = function(element) {
		return findParent(element);
	}
	
	function findParent(element) {
		var parent = null;
		var cssClasses = element.attr("class");
		if (element.attr("class")) {
			if (cssClasses.indexOf("tile") >= 0) {
				parent = element;
			} else if (cssClasses.indexOf("lane") >= 0) {
				parent = null; // Should not iterate outside of lane
			} else {
				parent = findParent(element.parent());
			}
		}
		else {
			parent = findParent(element.parent());
		}
		
		return parent;
	}
	
	function updateTasks(forceUpdate) {
		gitlabTask.list(forceUpdate, function(tasks) {
			$(".lane .tile").remove();
			for (var i in tasks) {
				var task = tasks[i];
				addTaskToStory(task);
			}
			
			addEditTaskListener();
			$('[data-toggle="tooltip"]').tooltip();
		});
	}
	
	function addEditTaskListener() {
		$(".tile.task a").off().on("click", function(event) {
			event.preventDefault();
			var task = $(this).parent().parent().parent().parent();
			var label = task.parent().data("label");
			var taskId = task.attr("id");
			gitlabTask.showEditDialog(taskId, label, function(task){
				updateTasks(true);
			});
		});
	}
	
	function addTaskToStory(task) {
		if ($('.story[data-story="' + task.taskData.storyId + '"]').length === 0) {
			return;
		}
		
		var storyClasses = $('.story[data-story="' + task.taskData.storyId + '"]').attr("class");
		var stateOpen = storyClasses && storyClasses.indexOf("closed") < 0;
		
		var lane = getLaneLabel(task);
		if (lane) {
			task.labels.splice(lane.index, 1);
		} else {
			lane = {
				title: GITLAB.todoLaneName
			};
		}
		
		var laneLabel = lane.title;
		if (!stateOpen && laneLabel !== getLastLane().data("name")) {
			laneLabel = getLastLane().data("name");
			// The story is closed - move all tasks to last lane and update
			gitlabTask.setParent(task.id, task.taskData.parentTaskId, task.taskData.storyId, laneLabel);
		}
		
		
		var addToStoryLane = $('.story[data-story="' + task.taskData.storyId + '"] .lane.' + laneLabel);
		
		var tile = getBoardTile(task, stateOpen);
		var added = false;
		if (addToStoryLane.children().length > 1) {
			// There are other tasks added. Check parentTaskId
			for (var i = 1; i < addToStoryLane.children().length; i++) {
				var t = addToStoryLane.children()[i];
				var parentTaskId = $(t).data("parenttaskid");
				if (parentTaskId === task.id) {
					added = true;
					$(tile).insertBefore($(t));
					break;
				}
			}
		}
		
		if (!added) {
			$(tile).appendTo(addToStoryLane);
		}
	}
	
	function getLaneLabel(task) {
		var lanes = $(".board .titles div");
		for (var i in lanes) {
			for (var j in task.labels) {
				if (task.labels[j] === $(lanes[i]).data("name")) {
					return {
						index: parseInt(j),
						title: task.labels[j]
					};
				}
			}
		}
		
		return null;
	}
	
	function getLastLane() {
		return $(".board .titles div").last();
	}
	
	function clean() {
		var html = "";		
		html += '<div class="loading">Loading stories...</div>';
		html += '<div class="titles">';
		html += '</div>';		
		html += '<div id="stories">';
		html += '</div>';
		
		board.html(html);
	}

	function redraw(callback) {
		clean();
		addStories(board, GITLAB.projectId, GITLAB.milestone, function(){
			$("div.board .loading").hide();
			callback();
			updateTasks(false);
		});
	}

	function addStories(board, projectId, milestone, callback) {
		gitlabIssue.list(false, null, milestone, function(stories) {
			for (var s in stories) {
				var story = stories[s];
				$("#stories").append(getStory(story));
			}
			addLanes(board, projectId);
			callback();
		});
	}

	function getStory(task) {
		var story = "";
		story +='	<div class="story '+ task.state + '" data-story="' + task.id + '">';
		story +='		<div class="card">' + getBoardTile(task, false) + '</div>';
		story +='		<div class="lanes"></div>';
		story +='	</div>';
		return story;
	}

	function addLanes(board, projectId) {
		var result = gitlabBoard.list();		
		var data = result[0];
			
		addLane(board, {name: GITLAB.todoLaneName});
		for (var l in data.lists) {
			var label = data.lists[l];
			addLane(board, label.label);
		}
		
		// Add add task button
		$(board).find(".lanes .lane."+GITLAB.todoLaneName+" .toolbar").each(function(index, element){
			$(element).append('<button class="add">Add task</button>');
		});
		
		$(board).find(".lanes .lane."+GITLAB.todoLaneName+" .toolbar button.add").on("click", function(e){
			var lane = $(this).parent().parent();
			var label = lane.data("label");
			var storyId = lane.parent().parent().data("story");
			var parentTaskId = lane.children().last() ? lane.children().last().attr("id") : null;
			gitlabTask.showAddDialog(GITLAB.milestone, label, storyId, parentTaskId, function(task) {
				addTaskToStory(task);
				addEditTaskListener();
			});
		});
	}

	function getBoardTile(task, draggable) {

		var tile = "";
		
		var cssClass = "tile";
		cssClass += " " + task.state;
		
		var dataParent = "";
		var tileType = TileType.STORY;
		var url = task.web_url;
		
		
		var tileLabels = "";
		var backgroundColor = "";
		
		// Labels
		if (task.labels) {
			tileLabels += '<div class="labels">';
			var labels = gitlabLabel.list(null);
			
			// Set background color from first label
			var label = new gitlabLabel.fetchFromList(task.labels[0], labels);
			if (label) {
				backgroundColor = 'style="background-color: ' + label.color + '"';
			}
			
			// Draw labels
			for (var i in task.labels) {
				var l = task.labels[i];
				label = new gitlabLabel.fetchFromList(l, labels);
				if (label) {
					tileLabels += '<span class="' + new Helpers().getFontClass(label.color) + '" style="background-color: ' + label.color + '">' + l + '</span>';
				}
			}
			tileLabels += '</div>';
		}
		
		// Tile styling
		if (task.taskData) {
			tileType = TileType.TASK;
			cssClass += " task";
			
			if (task.taskData.parentTaskId) {
				dataParent = ' data-parentTaskId="' + task.taskData.parentTaskId + '"';
			}
		}
		else {
			tileType = TileType.STORY;
		}
		
		var dragData = '';
		if (draggable) {
			dragData = ' draggable="true" ondragstart="drag(event)"';
		}
		tile += '<div id="' + task.id + '" class="' + cssClass + '"'+dataParent+' ' + dragData + ' ' + backgroundColor + '><div>';
		
		tile += '<div class="titleContainer">';
		tile += '  <div class="id">#' + task.iid + '</div>';
		tile += '  <div class="title">';
		tile += '    <a href="' + url + '" data-toggle="tooltip" data-placement="top" title="' + task.title + '">' + task.title + '</a>'
		tile += '  </div>';
		tile += '</div>';
		
		if (task.assignee) {
			tile += '<div class="assignee user"><img class="avatar" src="' + task.assignee.avatar_url + '">' + task.assignee.name + '</div>';
		}
		
		tile += tileLabels;
		
		if (task.timeStatistics && task.timeStatistics.human_time_estimate) {
			tile += '<div class="estimatedTime">' + task.timeStatistics.human_time_estimate + '</div>';
		}
		
		tile += '</div></div>';
		return tile;
	}

	function addLane(board, label) {
		var style = "";
		if (label.color) {
			style += 'style="border-top-color:' + label.color + '"';
		}
		board.find(".titles").append('<div data-name="' + label.name + '" ' + style + '><h3>' + label.name + "</h3></div>");
		board.find(".story").each(function(index, element){
			$(element).find(".lanes").append(getLane(label.name, label.name));
		});
	}

	function getLane(className, label) {
		var lane = "";
		lane += '<div class="lane ' + className + '" data-label=' + label +' ondrop="drop(event)" ondragover="allowDrop(event)">'; 
		lane += '	<div class="toolbar"></div>';
		lane += '</div>';
		return lane;
	}
}
